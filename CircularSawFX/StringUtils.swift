//
//  StringUtils.swift
//  CircularSawFX
//
//  Created by Joshua Foster on 10/6/15.
//  Copyright © 2015 Josh2112 Apps. All rights reserved.
//

import UIKit

extension String {
    func spannedHTMLString( fontName: String, fontSize: Int, fontColor: UIColor ) -> String {
        let colorHex = fontColor.toHexString()
        return "<span style='font-family: \(fontName); font-size: \(fontSize); color: \(colorHex)'>\(self)</span>"
    }
}

