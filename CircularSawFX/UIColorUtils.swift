//
//  UIColorUtils.swift
//  CircularSawFX
//
//  Created by Joshua Foster on 10/6/15.
//  Copyright © 2015 Josh2112 Apps. All rights reserved.
//

import UIKit

extension UIColor {
    func toHexString() -> NSString {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb)
    }
}