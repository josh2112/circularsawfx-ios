//
//  AVQueuePlayerTester.swift
//  CircularSawFX
//
//  Created by Joshua Foster on 10/2/15.
//  Copyright (c) 2015 Josh2112 Apps. All rights reserved.
//
import Foundation
import AVFoundation
import Swift

class ChainsawSoundPlayer {
    
    var accelSound: AVPlayerItem?
    var fullThrottleSound: AVPlayerItem?
    var fullThrottleSound2: AVPlayerItem?
    var decelSound: AVPlayerItem?
    
    var queuePlayer = AVQueuePlayer()
    
    init() {
        accelSound = loadSound( "circularsaw_accel" )
        fullThrottleSound = loadSound( "circularsaw_fullthrottle", notifyWhenFinished: true )
        fullThrottleSound2 = loadSound( "circularsaw_fullthrottle", notifyWhenFinished: true )
        decelSound = loadSound( "circularsaw_decel" )
    }
    
    func accel() {
        queuePlayer.removeAllItems()
        
        [accelSound, fullThrottleSound, fullThrottleSound2].forEach {
            $0!.seekToTime( kCMTimeZero )
            queuePlayer.insertItem( $0!, afterItem: nil )
        }
        
        queuePlayer.play()
    }
    
    lazy var accelSoundDuration: Double = {
        [unowned self] in CMTimeGetSeconds( self.accelSound!.asset.duration )
    }()
    
    lazy var decelSoundDuration: Double = {
        [unowned self] in CMTimeGetSeconds( self.decelSound!.asset.duration )
    }()
    
    func decel() -> Double {
        let proportion = CMTimeGetSeconds( accelSound!.currentTime() ) /
            CMTimeGetSeconds( accelSound!.asset.duration );
        
        // We need to remove all the items past the current item,
        // then append the decel sound, then advance to it.
        let sounds = queuePlayer.items()
        let removeStartIdx = sounds.indexOf(queuePlayer.currentItem! )! + 1
        sounds[removeStartIdx..<sounds.count].forEach { queuePlayer.removeItem( $0 ) }
        
        let decelDistanceFromEndSeconds = proportion * decelSoundDuration;
        
        let decelSeekTime = CMTimeMakeWithSeconds(
            decelSoundDuration - decelDistanceFromEndSeconds, Int32(NSEC_PER_SEC) )
        
        decelSound?.seekToTime( decelSeekTime )
        queuePlayer.insertItem( decelSound!, afterItem: nil )
        queuePlayer.advanceToNextItem()
        
        return decelDistanceFromEndSeconds
    }
    
    func stopAll() {
        queuePlayer.pause()
        queuePlayer.seekToTime( CMTimeMakeWithSeconds( 0, Int32(NSEC_PER_SEC) ) )
    }
    
    func loadSound( filename: String, notifyWhenFinished: Bool = false ) -> AVPlayerItem? {
        let url = NSBundle.mainBundle().URLForResource( filename, withExtension: "wav" )
        guard url != nil else {
            print( "Could not load sound '\(filename)'" )
            return nil
        }
        
        let sound = AVPlayerItem( URL: url! )
        
        if notifyWhenFinished {
            NSNotificationCenter.defaultCenter().addObserver( self, selector: "loopFullThrottle:",
                name: AVPlayerItemDidPlayToEndTimeNotification, object: sound )
        }
        
        return sound
    }
    
    @objc func loopFullThrottle( note: NSNotification ) {
        let sound = loadSound( "circularsaw_fullthrottle", notifyWhenFinished: true )
        queuePlayer.insertItem( sound!, afterItem: nil )
    }
}