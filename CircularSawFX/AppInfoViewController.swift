//
//  AppInfoViewController.swift
//  CircularSawFX
//
//  Created by Joshua Foster on 10/6/15.
//  Copyright © 2015 Josh2112 Apps. All rights reserved.
//

import UIKit

class AppInfoViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var creditsView: UIWebView?;
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let creditsHtml = NSLocalizedString( "soundCreditHtml", comment: "" )
        creditsView!.delegate = self
        creditsView!.loadHTMLString( creditsHtml, baseURL: nil )
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.URL where navigationType == UIWebViewNavigationType.LinkClicked {
            UIApplication.sharedApplication().openURL( url )
            return false
        }
        return true
    }
}