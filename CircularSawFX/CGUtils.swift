//
//  CGUtils.swift
//  CircularSawFX
//
//  Created by Joshua Foster on 10/2/15.
//  Copyright (c) 2015 Josh2112 Apps. All rights reserved.
//

import UIKit

extension CGPath {
    class func fromCircle( origin: CGPoint, radius: Double ) -> CGPath {
        return fromCircle( Double(origin.x), y: Double(origin.y), radius: radius );
    }

    class  func fromCircle( x: Double, y: Double, radius: Double ) -> CGPath {
        return UIBezierPath( ovalInRect: CGRect( x: x-radius, y: y-radius,
            width: radius*2, height: radius*2 ) ).CGPath
    }
}

extension CGPoint {
    func distanceTo( pt: CGPoint ) -> CGFloat {
        return distanceTo( pt.x, y: pt.y )
    }
    func distanceTo( x: CGFloat, y: CGFloat ) -> CGFloat {
        return hypot( abs( x - self.x ), abs( y - self.y ) )
    }
}

extension CGRect {
    func corners() -> [CGPoint] {
        return [
            CGPointMake( self.minX, self.minY ),
            CGPointMake( self.minX, self.maxY ),
            CGPointMake( self.maxX, self.minY ),
            CGPointMake( self.maxX, self.maxY )
        ]
    }
}