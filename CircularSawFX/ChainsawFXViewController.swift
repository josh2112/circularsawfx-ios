//
//  ChainsawFXViewController.swift
//  CircularSawFX
//
//  Created by Joshua Foster on 10/1/15.
//  Copyright (c) 2015 Josh2112 Apps. All rights reserved.
//

import UIKit

class ChainsawFXViewController: UIViewController {
    
    let pathAnimationKey = "path"
    
    @IBOutlet var overlay: UILabel?;

    var player = ChainsawSoundPlayer()
    
    var lastTapPoint: CGPoint?
    var currentAnimation: CABasicAnimation?
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //player.queuePlayer.volume = 0.02
        
        let mask = CAShapeLayer()
        mask.path = UIBezierPath( rect: CGRectZero ).CGPath
        mask.backgroundColor = UIColor.blackColor().CGColor
        overlay!.layer.mask = mask
        
        NSNotificationCenter.defaultCenter().addObserver( self,
            selector: "applicationWillResignActive:",
            name: UIApplicationWillResignActiveNotification,
            object: nil )
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        player.accel()
        lastTapPoint = touches.first!.locationInView( self.view )
        doRevealAnimation()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let decelDuration = player.decel()
        doHideAnimation( decelDuration )
    }
    
    func doRevealAnimation() {
        let startPath = CGPath.fromCircle( lastTapPoint!, radius: 0 )
        
        let largestDistanceToCorner = Double( view!.bounds.corners().map {
            lastTapPoint!.distanceTo( $0 ) }.maxElement()! )
        
        let anim = CABasicAnimation( keyPath: pathAnimationKey )
        anim.fromValue = startPath
        anim.duration = player.accelSoundDuration
        
        overlayMaskLayer().path = CGPath.fromCircle( lastTapPoint!, radius: largestDistanceToCorner )
        overlayMaskLayer().addAnimation( anim, forKey: pathAnimationKey )
    }
    
    func overlayMaskLayer() -> CAShapeLayer {
        return overlay!.layer.mask as! CAShapeLayer
    }
    
    func overlayMaskLayerPresentation() -> CAShapeLayer {
        return overlayMaskLayer().presentationLayer() as! CAShapeLayer
    }
    
    func doHideAnimation( duration: Double ) {
        let currentPath = overlayMaskLayerPresentation().path
        
        let anim = CABasicAnimation( keyPath: pathAnimationKey )
        anim.fromValue = currentPath
        anim.duration = duration
        
        overlayMaskLayer().path = CGPath.fromCircle( lastTapPoint!, radius: 0 )
        overlayMaskLayer().addAnimation( anim, forKey: pathAnimationKey )
    }
    
    func applicationWillResignActive( note: NSNotification ) {
        player.stopAll()
    }
    
}